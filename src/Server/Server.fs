open System.IO

open Microsoft.Extensions.DependencyInjection
open FSharp.Control.Tasks.V2

open Giraffe
open Saturn
open Saturn.CSRF.View
open Thoth.Json.Net

open Shared

let publicPath = Path.GetFullPath "../Client/public"
let port = 8085us
let getInitialGeoJson = Path.GetFullPath "./geojson.json" |> File.ReadAllTextAsync
let getGeojsonStorage = Path.GetFullPath "./geojsonStorage.json" |> File.ReadAllTextAsync

let webApp = router {
    get "/api/init" (fun next ctx ->
        task {
            let! initialGeoJson = getInitialGeoJson
            let InitialGeoObject = Decode.Auto.fromString<FeatureCollectionDto> initialGeoJson

            let response =
                match InitialGeoObject with
                | Ok obj -> Successful.OK obj next ctx
                | Error e -> ServerErrors.INTERNAL_ERROR e next ctx

            return! response
        })
    get "/api/polygons" (fun next ctx ->
        task {
            let! geoJson = getGeojsonStorage
            let geoJsonObject = Decode.Auto.fromString<FeatureCollectionDto> geoJson

            let response =
                match geoJsonObject with
                | Ok obj -> Successful.OK obj next ctx
                | Error e -> ServerErrors.INTERNAL_ERROR e next ctx

            return! response
        })

    post "/api/polygons" (fun next ctx ->
        task {
            let! res = ctx.BindJsonAsync<FeatureCollectionDto> ()
            let resJson = Encode.Auto.toString(4, res)

            do! File.WriteAllTextAsync(Path.GetFullPath "./geojsonStorage.json", resJson)

            return! Successful.OK "" next ctx
        })

}

let configureSerialization (services:IServiceCollection) =
    Thoth.Json.Giraffe.ThothSerializer ()
    |> services.AddSingleton<Giraffe.Serialization.Json.IJsonSerializer>

let app = application {
    url ("http://0.0.0.0:" + port.ToString() + "/")
    use_router webApp
    memory_cache
    use_static publicPath
    service_config configureSerialization
    use_gzip
}

run app
