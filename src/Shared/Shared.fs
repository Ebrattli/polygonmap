namespace Shared

type GeoJsonTypeDto =
| FeatureCollection
| Feature

type GeometryTypeDto =
| Polygon

type GeometryDto = {
    ``type``: GeometryTypeDto
    coordinates: float [] [] []
}

type FeatureDto = {
    ``type``: GeoJsonTypeDto
    geometry: GeometryDto
    id: string
}


type FeatureCollectionDto = {
    ``type``: GeoJsonTypeDto
    features: FeatureDto []
}