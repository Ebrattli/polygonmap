module PolygonMap.DtoToGeojson

open Fable.Core.JsInterop
open Shared

let featureToGeojsonFeature feature =
    let obj = createEmpty<Geojson.Feature<Geojson.Polygon,unit>>
    obj.geometry <- createEmpty<Geojson.Polygon>
    obj.geometry.``type`` <- "Polygon"
    obj.geometry.coordinates <- !!feature.geometry.coordinates
    obj.``type`` <- "Feature"
    obj.properties <- createEmpty
    obj.id <- Some !^feature.id
    obj