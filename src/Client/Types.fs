module PolygonMap.Types

open Fable.Import.React
open Leaflet

open Shared


// The model holds data that you want to keep track of while the application is running
// in this case, we are keeping track of a counter
// we mark it as optional, because initially it will not be available from the client
// the initial value will be requested from server
type Model = {
    featureCollection: FeatureCollectionDto
    selectedElementIds: string list
}
// The Msg type defines what events/actions can occur while the application is running
// the state of the application changes *only* in reaction to these events
type Msg =
| InitialGeoJsonLoaded of Result<FeatureCollectionDto, exn>
| PolygonClickedEvent of LeafletMouseEvent
| Union of MouseEvent
| Intersect of MouseEvent
