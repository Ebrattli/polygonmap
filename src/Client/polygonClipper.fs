module PolygonMap.PolygonClipper

type [<AllowNullLiteral>] IPolygonClipper =
    abstract intersection: float [] [] [] -> float [] [] [] [] -> float [] [] [] []
    abstract union: float [] [] [] -> float [] [] [] [] -> float [] [] [] []