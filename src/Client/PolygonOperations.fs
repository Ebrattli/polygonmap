module PolygonMap.PolygonOperations

open System

open Fable.Core.Exceptions
open Fable.Core

open Shared
open Types
open PolygonClipper

let [<Import("default", "polygon-clipping")>] clipper: IPolygonClipper = jsNative

let polygonOperation (model: Model) (operation: float [] [] [] -> float [] [] [] [] -> float [] [] [] []) =

    let selectedFeaturesId = model.selectedElementIds
    let selectedFeatures =
        Array.filter
            (fun feature -> List.contains feature.id selectedFeaturesId)
                model.featureCollection.features

    let selectedFeaturesCoordinates =
        Array.map (fun feat -> feat.geometry.coordinates) selectedFeatures

    let res =
        operation
        <| Array.head selectedFeaturesCoordinates
        <| Array.tail selectedFeaturesCoordinates

    let createFeatureFromCoordinates (coordinates: float [] [] []) =
        {
            ``type`` = Feature
            id = (Guid.NewGuid ()).ToString ()
            geometry = {
                ``type`` = Polygon
                coordinates = coordinates
            }
        }

    let newFeatures =
        if res = Array.empty then None
        else Some (Array.map createFeatureFromCoordinates res)

    let filteredFeatures =
        Array.filter
            ((fun feature -> List.contains feature.id selectedFeaturesId) >> not)
                model.featureCollection.features

    let features =
        match newFeatures with
        | Some features -> Array.append features filteredFeatures
        | None -> filteredFeatures

    let featureCollection = {model.featureCollection with features = features}

    { model with featureCollection = featureCollection; selectedElementIds = [] }


let polygonUnion (model: Model) = polygonOperation model clipper.union
let polygonIntersection (model: Model) = polygonOperation model clipper.intersection