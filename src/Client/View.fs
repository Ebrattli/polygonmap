module PolygonMap.View

open Fable.Helpers.React
open Fable.Helpers.React.Props
open Fable.Core.JsInterop
open Fable.Import

open Fulma

open ReactLeaflet

open PolygonMap.Types
open Shared
open DtoToGeojson

let safeComponents =
    let components =
        span [ ]
           [
             a [ Href "https://saturnframework.github.io" ] [ str "Saturn" ]
             str ", "
             a [ Href "http://fable.io" ] [ str "Fable" ]
             str ", "
             a [ Href "https://elmish.github.io/elmish/" ] [ str "Elmish" ]
             str ", "
             a [ Href "https://mangelmaxime.github.io/Fulma" ] [ str "Fulma" ]
           ]

    p [ ]
        [ strong [] [ str "SAFE Template" ]
          str " powered by: "
          components ]

let view (model : Model) (dispatch : Msg -> unit) =

    let createGeoJsonFeatureView (feature: FeatureDto) =
        let selectedStyle = createObj [
            "color" ==> "#f4425f"
        ]
        let deafaultStyle = createObj [
            "color" ==> "#3388ff"
        ]

        let styleFunc (feature: Geojson.Feature<Geojson.GeometryObject,_>) =
            if List.contains (!!feature.id) model.selectedElementIds
            then selectedStyle
            else deafaultStyle

        let geojsonFeature = featureToGeojsonFeature feature
        Browser.console.log geojsonFeature

        ReactLeaflet.geoJSON [
            GeoJSONProps.Key feature.id
            GeoJSONProps.Data geojsonFeature
            GeoJSONProps.Style !!styleFunc
            GeoJSONProps.OnClick (fun ev -> dispatch (PolygonClickedEvent ev))
        ] []

    let featuresView =
        Array.map createGeoJsonFeatureView model.featureCollection.features

    let polygonOperationsView =
        let cantDoOperation = model.selectedElementIds.Length < 2
        div [] [
            Button.button [
                Button.Option.Disabled cantDoOperation
                Button.Option.OnClick (fun ev -> dispatch (Union ev))
            ] [ str "Union" ]
            Button.button [
                Button.Option.Disabled cantDoOperation
                Button.Option.OnClick (fun ev -> dispatch (Intersect ev))
            ] [ str "Intersection" ]
        ]

    let leafletMap =
        ReactLeaflet.map [
            MapProps.Center !^ (51.5086, -0.125)
            MapProps.SetView true
            MapProps.Zoom (float 14)
            MapProps.ZoomSnap 0.1
            MapProps.Id "myMap"
            MapProps.Style [ CSSProp.Height "500px" ]
        ] [
            yield tileLayer [
                TileLayerProps.Attribution "&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors"
                TileLayerProps.Url "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            ] []

            yield! featuresView
        ]


    div [] [
        Navbar.navbar [ Navbar.Color IsPrimary ] [
            Navbar.Item.div [ ] [
                Heading.h2 [ ] [
                    str "Polygons on a map"
                ]
            ]
        ]

        leafletMap
        polygonOperationsView

        Footer.footer [ ] [
            Content.content [ Content.Modifiers [ Modifier.TextAlignment (Screen.All, TextAlignment.Centered) ] ] [
                safeComponents ]
        ]
    ]
