module PolygonMap.State

open System
open Microsoft.FSharp.Core

open Elmish

open Fable.Core.JsInterop
open Fable.PowerPack.Fetch
open Fable.PowerPack.PromiseImpl

open Thoth.Json

open Types
open Shared
open PolygonOperations

let createFeatureWithId (feature: FeatureDto) =
    {
        ``type`` = Feature
        geometry = feature.geometry
        id = (Guid.NewGuid ()).ToString ()
    }


// The update function computes the next state of the application based on the current state and the incoming events/messages
// It can also run side-effects (encoded as commands) like calling the server via Http.
// these commands in turn, can dispatch messages to which the update function will react.
let update (msg : Msg) (currentModel : Model) : Model * Cmd<Msg> =
    match msg with
    | InitialGeoJsonLoaded maybeDto ->
        let featureCollection =
            match maybeDto with
            | Ok dto -> { dto with features = Array.map createFeatureWithId dto.features }
            | Error _-> {
                ``type`` = FeatureCollection
                features = [||] }

        let newModel = { currentModel with featureCollection = featureCollection }
        newModel, Cmd.none
    | PolygonClickedEvent ev ->
        let id' = !!(ev?target?options?data?id)
        let selectedElements =
            if List.contains id' currentModel.selectedElementIds
            then List.filter (fun el -> el <> id') currentModel.selectedElementIds
            else id' :: currentModel.selectedElementIds


        let newModel =
            {currentModel with selectedElementIds = selectedElements}
        newModel, Cmd.none
    | Union _ ->
        let newModel = polygonUnion currentModel

        promise {
            let! _ = postRecord "/api/polygons" newModel.featureCollection []
            ()
        } |> ignore

        newModel, Cmd.none

    | Intersect _ ->
        let newModel = polygonIntersection currentModel
        promise {
            let! _ = postRecord "/api/polygons" newModel.featureCollection []
            ()
        } |> ignore


        newModel, Cmd.none


let initialGeoJson = fetchAs<FeatureCollectionDto> "/api/init" (Decode.Auto.generateDecoder ())

// defines the initial state and initial command (= side-effect) of the application
let init () : Model * Cmd<Msg> =
    let loadGeoJsonCmd =
        Cmd.ofPromise
            initialGeoJson
            []
            (Ok >> InitialGeoJsonLoaded)
            (Error >> InitialGeoJsonLoaded)
    let initialModel = {
        featureCollection = { ``type`` = FeatureCollection; features = [||] }
        selectedElementIds = [] }
    initialModel, loadGeoJsonCmd